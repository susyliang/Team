package model

// Index page for this app.
var Index = `<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=3, shrink-to-fit=no">
		<title>团队协作系统</title>
		<link rel="shortcut icon" href="/view/dist/app.ico" />
	</head>
	<body>
		<div id="app"></div>
		<script src="/view/dist/app.js"></script>
	</body>
</html>
`
